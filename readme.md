# Рабочий процесс

![GitIcon]

## Подготовка сервера
- выберем подходящее имя домена подобно *test.cetacs.com
- создаем домен
> либо добавим поддомен в cloudflare 
>
> либо добавим домен локально в hosts
- в [FastPanel]е создаем сайт указывая выбранное доменое имя

## Подготовка рабочего места
- создаем git репозиторий удаленно
- клонируем git локально
- настраиваем шторм используя данные от предыдущего шага

## Разворот проекта
- создаем бекап на рабочем сайте
- создаем файл restore на сервере
- запускаем домен в браузере кликаем

## Рабочий процесс
- для каждой задачи создается ветка от основной ветки
- задачу дробим на микроподзадачи
- решаем микроподзадачу доводим до логического (рабочего состояния)
- используя gitg или шторм создаем коммиты состояния
- пушим на сервер 

## Полезные ссылки

| Ссылки |
| ------ |
| [git репозиторий проектов cetacs][CetacsBitbucker] |
| [Разворот сайта БУС из копии][BitrixRestore] |
| [гуглим учимся использовать git][GitPhpstorm] |


https://proglib.io/p/git-for-half-an-hour

https://support.atlassian.com/bitbucket-cloud/docs/create-a-git-repository/

**Дополняем инструкцию**

[FastPanel]: <https://192.168.0.204:8888/#/about>
[CetacsBitbucker]: <https://bitbucket.org/cetacs/>
[BitrixRestore]: <https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=35&LESSON_ID=6979>
[GitPhpstorm]: <https://www.youtube.com/watch?v=iQqDce_9y3k>

[GitIcon]: <https://cdn.iconscout.com/icon/free/png-256/git-free-opensource-distributed-version-control-system-square-46231.png>

